﻿
namespace oop_lab1
{
    partial class PositionGuesser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PositionGuesser));
            this.btnNorthWest = new System.Windows.Forms.Button();
            this.btnNorth = new System.Windows.Forms.Button();
            this.btnNorthEast = new System.Windows.Forms.Button();
            this.btnWest = new System.Windows.Forms.Button();
            this.btnEast = new System.Windows.Forms.Button();
            this.btnSouthWest = new System.Windows.Forms.Button();
            this.btnSouth = new System.Windows.Forms.Button();
            this.btnSouthEast = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.label_description = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.label_title_guessedPositions = new System.Windows.Forms.Label();
            this.btnEnd = new System.Windows.Forms.Button();
            this.label_PreviousGuesses = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnNorthWest
            // 
            this.btnNorthWest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(224)))), ((int)(((byte)(173)))));
            this.btnNorthWest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNorthWest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNorthWest.Location = new System.Drawing.Point(34, 177);
            this.btnNorthWest.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnNorthWest.Name = "btnNorthWest";
            this.btnNorthWest.Size = new System.Drawing.Size(101, 63);
            this.btnNorthWest.TabIndex = 0;
            this.btnNorthWest.Text = "Northwest";
            this.btnNorthWest.UseVisualStyleBackColor = false;
            this.btnNorthWest.Click += new System.EventHandler(this.btnNorthWest_Click);
            // 
            // btnNorth
            // 
            this.btnNorth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(173)))), ((int)(((byte)(168)))));
            this.btnNorth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNorth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNorth.Location = new System.Drawing.Point(142, 177);
            this.btnNorth.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnNorth.Name = "btnNorth";
            this.btnNorth.Size = new System.Drawing.Size(101, 63);
            this.btnNorth.TabIndex = 1;
            this.btnNorth.Text = "North";
            this.btnNorth.UseVisualStyleBackColor = false;
            this.btnNorth.Click += new System.EventHandler(this.btnNorth_Click);
            // 
            // btnNorthEast
            // 
            this.btnNorthEast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(224)))), ((int)(((byte)(173)))));
            this.btnNorthEast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNorthEast.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNorthEast.Location = new System.Drawing.Point(250, 177);
            this.btnNorthEast.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnNorthEast.Name = "btnNorthEast";
            this.btnNorthEast.Size = new System.Drawing.Size(101, 63);
            this.btnNorthEast.TabIndex = 2;
            this.btnNorthEast.Text = "Northeast";
            this.btnNorthEast.UseVisualStyleBackColor = false;
            this.btnNorthEast.Click += new System.EventHandler(this.btnNorthEast_Click);
            // 
            // btnWest
            // 
            this.btnWest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(173)))), ((int)(((byte)(168)))));
            this.btnWest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnWest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWest.Location = new System.Drawing.Point(34, 245);
            this.btnWest.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnWest.Name = "btnWest";
            this.btnWest.Size = new System.Drawing.Size(101, 63);
            this.btnWest.TabIndex = 3;
            this.btnWest.Text = "West";
            this.btnWest.UseVisualStyleBackColor = false;
            this.btnWest.Click += new System.EventHandler(this.btnWest_Click);
            // 
            // btnEast
            // 
            this.btnEast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(173)))), ((int)(((byte)(168)))));
            this.btnEast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEast.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEast.Location = new System.Drawing.Point(250, 245);
            this.btnEast.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEast.Name = "btnEast";
            this.btnEast.Size = new System.Drawing.Size(101, 63);
            this.btnEast.TabIndex = 4;
            this.btnEast.Text = "East";
            this.btnEast.UseVisualStyleBackColor = false;
            this.btnEast.Click += new System.EventHandler(this.btnEast_Click);
            // 
            // btnSouthWest
            // 
            this.btnSouthWest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(224)))), ((int)(((byte)(173)))));
            this.btnSouthWest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSouthWest.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSouthWest.Location = new System.Drawing.Point(34, 312);
            this.btnSouthWest.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSouthWest.Name = "btnSouthWest";
            this.btnSouthWest.Size = new System.Drawing.Size(101, 63);
            this.btnSouthWest.TabIndex = 5;
            this.btnSouthWest.Text = "Southwest";
            this.btnSouthWest.UseVisualStyleBackColor = false;
            this.btnSouthWest.Click += new System.EventHandler(this.btnSouthWest_Click);
            // 
            // btnSouth
            // 
            this.btnSouth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(173)))), ((int)(((byte)(168)))));
            this.btnSouth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSouth.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSouth.Location = new System.Drawing.Point(142, 312);
            this.btnSouth.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSouth.Name = "btnSouth";
            this.btnSouth.Size = new System.Drawing.Size(103, 63);
            this.btnSouth.TabIndex = 6;
            this.btnSouth.Text = "South";
            this.btnSouth.UseVisualStyleBackColor = false;
            this.btnSouth.Click += new System.EventHandler(this.btnSouth_Click);
            // 
            // btnSouthEast
            // 
            this.btnSouthEast.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(224)))), ((int)(((byte)(173)))));
            this.btnSouthEast.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSouthEast.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSouthEast.Location = new System.Drawing.Point(250, 312);
            this.btnSouthEast.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnSouthEast.Name = "btnSouthEast";
            this.btnSouthEast.Size = new System.Drawing.Size(101, 63);
            this.btnSouthEast.TabIndex = 7;
            this.btnSouthEast.Text = "Southeast";
            this.btnSouthEast.UseVisualStyleBackColor = false;
            this.btnSouthEast.Click += new System.EventHandler(this.btnSouthEast_Click);
            // 
            // btnReset
            // 
            this.btnReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnReset.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReset.Location = new System.Drawing.Point(140, 245);
            this.btnReset.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(101, 63);
            this.btnReset.TabIndex = 8;
            this.btnReset.Text = "RESET";
            this.btnReset.UseVisualStyleBackColor = false;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // label_description
            // 
            this.label_description.AutoSize = true;
            this.label_description.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_description.Location = new System.Drawing.Point(9, 18);
            this.label_description.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_description.Name = "label_description";
            this.label_description.Size = new System.Drawing.Size(681, 80);
            this.label_description.TabIndex = 11;
            this.label_description.Text = resources.GetString("label_description.Text");
            this.label_description.Click += new System.EventHandler(this.label_description_Click);
            // 
            // btnStart
            // 
            this.btnStart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(34, 110);
            this.btnStart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(101, 63);
            this.btnStart.TabIndex = 12;
            this.btnStart.Text = "START";
            this.btnStart.UseVisualStyleBackColor = false;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // label_title_guessedPositions
            // 
            this.label_title_guessedPositions.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(121)))), ((int)(((byte)(128)))));
            this.label_title_guessedPositions.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_title_guessedPositions.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.label_title_guessedPositions.Location = new System.Drawing.Point(376, 120);
            this.label_title_guessedPositions.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_title_guessedPositions.Name = "label_title_guessedPositions";
            this.label_title_guessedPositions.Size = new System.Drawing.Size(240, 41);
            this.label_title_guessedPositions.TabIndex = 13;
            this.label_title_guessedPositions.Text = "Guessed Positions";
            this.label_title_guessedPositions.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_title_guessedPositions.Click += new System.EventHandler(this.label_title_guessedPositions_Click);
            // 
            // btnEnd
            // 
            this.btnEnd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btnEnd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnd.Location = new System.Drawing.Point(250, 379);
            this.btnEnd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(101, 67);
            this.btnEnd.TabIndex = 14;
            this.btnEnd.Text = "Found it!";
            this.btnEnd.UseVisualStyleBackColor = false;
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // label_PreviousGuesses
            // 
            this.label_PreviousGuesses.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(84)))), ((int)(((byte)(121)))), ((int)(((byte)(128)))));
            this.label_PreviousGuesses.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_PreviousGuesses.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(233)))), ((int)(((byte)(233)))));
            this.label_PreviousGuesses.Location = new System.Drawing.Point(0, 0);
            this.label_PreviousGuesses.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_PreviousGuesses.Name = "label_PreviousGuesses";
            this.label_PreviousGuesses.Size = new System.Drawing.Size(223, 812);
            this.label_PreviousGuesses.TabIndex = 10;
            this.label_PreviousGuesses.Text = "No guesses yet.";
            this.label_PreviousGuesses.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label_PreviousGuesses.Click += new System.EventHandler(this.label_PreviousGuesses_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.label_PreviousGuesses);
            this.panel1.Location = new System.Drawing.Point(376, 162);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(240, 150);
            this.panel1.TabIndex = 15;
            // 
            // PositionGuesser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(228)))), ((int)(((byte)(248)))), ((int)(((byte)(246)))));
            this.ClientSize = new System.Drawing.Size(655, 458);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnEnd);
            this.Controls.Add(this.label_title_guessedPositions);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.label_description);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnSouthEast);
            this.Controls.Add(this.btnSouth);
            this.Controls.Add(this.btnSouthWest);
            this.Controls.Add(this.btnEast);
            this.Controls.Add(this.btnWest);
            this.Controls.Add(this.btnNorthEast);
            this.Controls.Add(this.btnNorth);
            this.Controls.Add(this.btnNorthWest);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "PositionGuesser";
            this.Text = "Position Guesser";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.PositionGuesser_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNorthWest;
        private System.Windows.Forms.Button btnNorth;
        private System.Windows.Forms.Button btnNorthEast;
        private System.Windows.Forms.Button btnWest;
        private System.Windows.Forms.Button btnEast;
        private System.Windows.Forms.Button btnSouthWest;
        private System.Windows.Forms.Button btnSouth;
        private System.Windows.Forms.Button btnSouthEast;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.Label label_description;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Label label_title_guessedPositions;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.Label label_PreviousGuesses;
        private System.Windows.Forms.Panel panel1;
    }
}