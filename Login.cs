﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CryptoLib;

namespace oop_lab1
{
    public partial class Login : Form
    {
        private int fl = 0;
        public Login()
        {
            InitializeComponent();
            //txtpsw.PasswordChar = '*';
        }

        public bool islogged { get; set; }
        private void btnlogin_Click(object sender, EventArgs e)
        {
            //int fl = 0;


            string pass = "";
            if (User.Instance.users.ContainsKey(txtname.Text))
            {
                User.Instance.users.TryGetValue(txtname.Text, out pass);
                string hashedPsw = CryptoLib.Encryptor.MD5Hash(txtpsw.Text);
                if(pass == hashedPsw)
                {
                    lbresult.Text = "Successful login!";
                    lbresult.ForeColor = Color.Green;
                    fl = 1;
                }
            }
            if (fl == 0) 
            {
                lbresult.Text = "Unsuccessful login!";
                lbresult.ForeColor = Color.Red;
            }
            else
            {
                timer1.Start();
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
            islogged = true;
            ApplicationSelector.GetForm.Show();
            timer1.Stop();
        }

        private void txtpsw_Enter(object sender, EventArgs e)
        {
            if (txtpsw.Text == "Password")
            {
                txtpsw.Text = "";
                txtpsw.ForeColor = Color.Black;
            }
        }

        private void txtpsw_Leave(object sender, EventArgs e)
        {
            if (txtpsw.Text == "")
            {
                txtpsw.Text = "Password";
                txtpsw.ForeColor = Color.DimGray;
            }
        }

        private void txtname_Leave(object sender, EventArgs e)
        {
            if (txtname.Text == "")
            {
                txtname.Text = "Username";
                txtname.ForeColor = Color.DimGray;
            }
        }

        private void txtname_Enter(object sender, EventArgs e)
        {
            if (txtname.Text == "Username")
            {
                txtname.Text = "";
                txtname.ForeColor = Color.Black;
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void SignUpLinkLabel_Enter(object sender, EventArgs e)
        {
           
        }

        private void SignUpLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SignUp signup = new SignUp();
            this.Hide();
            signup.ShowDialog();

        }

        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            if(fl != 1)
                Application.Exit();
        }

        private void txtname_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbresult_Click(object sender, EventArgs e)
        {

        }
    }
}
