﻿namespace oop_lab1
{
    partial class ApplicationSelector
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCalculator = new System.Windows.Forms.Button();
            this.btnEncryptor = new System.Windows.Forms.Button();
            this.btn_PositionGuesser = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCalculator
            // 
            this.btnCalculator.AutoSize = true;
            this.btnCalculator.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnCalculator.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCalculator.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnCalculator.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnCalculator.Location = new System.Drawing.Point(64, 25);
            this.btnCalculator.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalculator.Name = "btnCalculator";
            this.btnCalculator.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnCalculator.Size = new System.Drawing.Size(222, 111);
            this.btnCalculator.TabIndex = 0;
            this.btnCalculator.Text = "Calculator";
            this.btnCalculator.UseVisualStyleBackColor = false;
            this.btnCalculator.Click += new System.EventHandler(this.btnCalculator_Click);
            // 
            // btnEncryptor
            // 
            this.btnEncryptor.AutoSize = true;
            this.btnEncryptor.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btnEncryptor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEncryptor.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnEncryptor.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btnEncryptor.Location = new System.Drawing.Point(375, 25);
            this.btnEncryptor.Margin = new System.Windows.Forms.Padding(4);
            this.btnEncryptor.Name = "btnEncryptor";
            this.btnEncryptor.Size = new System.Drawing.Size(222, 111);
            this.btnEncryptor.TabIndex = 1;
            this.btnEncryptor.Text = "D/Encryptor";
            this.btnEncryptor.UseVisualStyleBackColor = false;
            this.btnEncryptor.Click += new System.EventHandler(this.btnEncryptor_Click);
            // 
            // btn_PositionGuesser
            // 
            this.btn_PositionGuesser.AutoSize = true;
            this.btn_PositionGuesser.BackColor = System.Drawing.SystemColors.HotTrack;
            this.btn_PositionGuesser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_PositionGuesser.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btn_PositionGuesser.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.btn_PositionGuesser.Location = new System.Drawing.Point(154, 183);
            this.btn_PositionGuesser.Margin = new System.Windows.Forms.Padding(4);
            this.btn_PositionGuesser.Name = "btn_PositionGuesser";
            this.btn_PositionGuesser.Size = new System.Drawing.Size(336, 111);
            this.btn_PositionGuesser.TabIndex = 2;
            this.btn_PositionGuesser.Text = "Position Guesser";
            this.btn_PositionGuesser.UseVisualStyleBackColor = false;
            this.btn_PositionGuesser.Click += new System.EventHandler(this.btn_PositionGuesser_Click);
            // 
            // ApplicationSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 345);
            this.Controls.Add(this.btn_PositionGuesser);
            this.Controls.Add(this.btnEncryptor);
            this.Controls.Add(this.btnCalculator);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "ApplicationSelector";
            this.Text = "ApplicationSelector";
            this.Load += new System.EventHandler(this.ApplicationSelector_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCalculator;
        private System.Windows.Forms.Button btnEncryptor;
        private System.Windows.Forms.Button btn_PositionGuesser;
    }
}