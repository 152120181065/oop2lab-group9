﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_lab1
{
    public partial class PositionGuesser : Form
    {
        private static PositionGuesser inst;
        int x = 512, y = 512, k = 1;
        int flag = 0;
        public static PositionGuesser GetForm
        {
            get
            {
                if(inst==null || inst.IsDisposed)
                {
                    inst = new PositionGuesser();
                }
                return inst;
            }
        }
        public PositionGuesser()
        {
            InitializeComponent();
        }

        private void PositionGuesser_Load(object sender, EventArgs e)
        {

        }

        private void btnNorth_Click(object sender, EventArgs e)
        {
            if (flag == 1)
            {
                int yy = PositionGuesserClass.North(y);
                label_PreviousGuesses.Text += "Location" + k + ": (" + x + "," + yy + ")\n";
                k++;
                y = yy;
            }
            else
                label_PreviousGuesses.Text += "\nPlease start the game first.\n";
        }
        private void btnNorthEast_Click(object sender, EventArgs e)
        {
            if (flag == 1)
            {
                int xx = PositionGuesserClass.East(x);
                int yy = PositionGuesserClass.North(y);
                label_PreviousGuesses.Text += "Location" + k + ": (" + xx + "," + yy + ")\n";
                k++;
                x = xx;
                y = yy;
            }
            else
                label_PreviousGuesses.Text += "\nPlease start the game first.\n";
        }
        private void btnEast_Click(object sender, EventArgs e)
        {
            if (flag == 1)
            {
                int xx = PositionGuesserClass.East(x);
                label_PreviousGuesses.Text += "Location" + k + ": (" + xx + "," + y + ")\n";
                k++;
                x = xx;
            }
            else
                label_PreviousGuesses.Text += "\nPlease start the game first.\n";
        }
        private void btnSouthEast_Click(object sender, EventArgs e)
        {
            if (flag == 1)
            {
                int xx = PositionGuesserClass.East(x);
                int yy = PositionGuesserClass.South(y);
                label_PreviousGuesses.Text += "Location" + k + ": (" + xx + "," + yy + ")\n";
                k++;
                x = xx;
                y = yy;
            }
            else
                label_PreviousGuesses.Text += "\nPlease start the game first.\n";
        }
        private void btnSouth_Click(object sender, EventArgs e)
        {
            if(flag==1)
            {
                int yy = PositionGuesserClass.South(y);
                label_PreviousGuesses.Text += "Location" + k + ": (" + x + "," + yy + ")\n";
                k++;
                y = yy;
            }
            else
                label_PreviousGuesses.Text += "\nPlease start the game first.\n";
        }
        private void btnSouthWest_Click(object sender, EventArgs e)
        {
            if (flag == 1)
            {
                int xx = PositionGuesserClass.West(x);
                int yy = PositionGuesserClass.South(y);
                label_PreviousGuesses.Text += "Location" + k + ": (" + xx + "," + yy + ")\n";
                k++;
                x = xx;
                y = yy;
            }
            else
                label_PreviousGuesses.Text += "\nPlease start the game first.\n";
        }
        private void btnWest_Click(object sender, EventArgs e)
        {
            if (flag == 1)
            {
                int xx = PositionGuesserClass.West(x);
                label_PreviousGuesses.Text += "Location" + k + ": (" + xx + "," + y + ")\n";
                k++;
                x = xx;
            }
            else
                label_PreviousGuesses.Text += "\nPlease start the game first.\n";
        }
        private void btnNorthWest_Click(object sender, EventArgs e)
        {
            if (flag == 1)
            {
                int xx = PositionGuesserClass.West(x);
                int yy = PositionGuesserClass.North(y);
                label_PreviousGuesses.Text += "Location" + k + ": (" + xx + "," + yy + ")\n";
                k++;
                x = xx;
                y = yy;
            }
            else
                label_PreviousGuesses.Text += "\nPlease start the game first.\n";
        }


        private void btnReset_Click(object sender, EventArgs e)
        {
            PositionGuesserClass.Reset();
            x = 512;
            y = 512;
            k = 1;
            label_PreviousGuesses.Text= "Location" + k + ": (" + x + "," + y + ")\n";
            k++;
        }

     

        private void label_PreviousGuesses_Click(object sender, EventArgs e)
        {

        }

        private void label_description_Click(object sender, EventArgs e)
        {

        }

        private void btnEnd_Click(object sender, EventArgs e)
        {
            label_PreviousGuesses.Text += "It was a pleasure trying to guess the position, to play again please click start :)";
            flag = 0;
        }

        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label_title_guessedPositions_Click(object sender, EventArgs e)
        {

        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            flag = 1; k = 1;
            x = 512; y = 512;
            PositionGuesserClass.Reset();
            label_PreviousGuesses.Text = "Game started.\nLocation" + k + ": (" + x + "," + y + ")\n";
            k++;
        }
    }
}
