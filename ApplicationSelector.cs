﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_lab1
{
    public partial class ApplicationSelector : Form
    {
        public ApplicationSelector()
        {
            InitializeComponent();
        }
        public bool isCalculatorClicked { get; set; }
        public bool isEncryptorClicked { get; set; }
        public bool isPositionGuesserClicked { get; set; }

        private static ApplicationSelector inst;
        public static ApplicationSelector GetForm
        {
            get
            {
                if (inst == null || inst.IsDisposed)
                    inst = new ApplicationSelector();
                return inst;

            }
        }

        private void btnCalculator_Click(object sender, EventArgs e)
        {
            this.Close();
            isCalculatorClicked = true;
            Calculator.GetForm.Show();        
        }

        private void btnEncryptor_Click(object sender, EventArgs e)
        {
            this.Close();
            isEncryptorClicked = true;
            Encryptor.GetForm.Show();
        }

        private void ApplicationSelector_Load(object sender, EventArgs e)
        {

        }

        private void btn_PositionGuesser_Click(object sender, EventArgs e)
        {
            this.Close();
            isPositionGuesserClicked = true;
            PositionGuesser.GetForm.Show();

        }
    }
}
