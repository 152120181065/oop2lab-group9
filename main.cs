﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_lab1
{
    static class main
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Login lgn = new Login();
            ApplicationSelector appslc = new ApplicationSelector();
            Application.Run(lgn);
            if (lgn.islogged)
                Application.Run(appslc);
            if (appslc.isCalculatorClicked)
                Application.Run(new Calculator());
            else if (appslc.isEncryptorClicked)
                Application.Run(new Encryptor());
            else if (appslc.isPositionGuesserClicked)
                Application.Run(new PositionGuesser());
        }
    }
}
