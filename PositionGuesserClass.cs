﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop_lab1
{
    public class PositionGuesserClass
    {
        private static int xx = 0, yy = 0;
        private static int x_max = 1024, x_min = 0, y_max = 1024, y_min = 0;
        public static int East(int x)
        {
            x_min = x;
            if (x_min == 1023)
                return x_max;
            xx = x_min + (x_max - x_min) / 2;
            return xx;
        }
        public static int South(int y)
        {
            y_max = y;
            if (y_max == 1)
                return y_min;
            yy = y_max - (y_max-y_min) / 2;
            return yy;
        }
        public static int West(int x)
        {
            x_max = x;
            if (x_max == 1)
                return x_min;
            xx = (x_max + x_min) / 2;
            return xx;
        }
        public static int North(int y)
        {
            y_min = y;
            if (y_min == 1023)
                return y_max;
            yy = y_min + (y_max - y_min) / 2;
            return yy;
        }
        public static void Reset()
        {
            xx = 0; x_max = 1024; x_min = 0; yy = 0; y_max = 1024; y_min = 0;
        }

    }
}
