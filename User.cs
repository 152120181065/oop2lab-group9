﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Windows.Forms;

namespace oop_lab1
{
    public sealed class User
    {
        private static User instance = null;
        private static readonly object padlock = new object();
        public Dictionary<string, string> users = new Dictionary<string, string>();
        User()
        {
            users.Add("abc", "202cb962ac59075b964b07152d234b70");//password:123
            users.Add("def", "250cf8b51c773f3f8dc8b4be867a9a02");//password:456
            users.Add("abcdef", "e10adc3949ba59abbe56e057f20f883e");//password:123456
        }
        public static User Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new User();
                    }
                    return instance;
                }
            }
        }
    }
}