﻿using CryptoLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_lab1
{
    public partial class SignUp : Form
    {
        public SignUp()
        {
            InitializeComponent();
        }

        private void SignUp_Load(object sender, EventArgs e)
        {

        }

        private void SignUpButton_Click(object sender, EventArgs e)
        {
            if(Password1TextBox.Text != Password2TextBox.Text && User.Instance.users.ContainsKey(UserNameTextBox.Text))
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "Your password must be same and the username is not available.";
                Password1TextBox.ForeColor = System.Drawing.Color.Red;
                Password2TextBox.ForeColor = System.Drawing.Color.Red;
                UserNameTextBox.ForeColor = System.Drawing.Color.Red;
                lblResult.Text = "";

            }
            else if (Password1TextBox.Text != Password2TextBox.Text)
            { 
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "Your password must be same.";
                Password1TextBox.ForeColor = System.Drawing.Color.Red;
                Password2TextBox.ForeColor = System.Drawing.Color.Red;
                UserNameTextBox.ForeColor = System.Drawing.Color.Black;
                lblResult.Text = "";
            }
            else if(User.Instance.users.ContainsKey(UserNameTextBox.Text))
            {
                lblError.ForeColor = System.Drawing.Color.Red;
                lblError.Text = "Username is not available";
                UserNameTextBox.ForeColor = System.Drawing.Color.Red;
                Password1TextBox.ForeColor = System.Drawing.Color.Black;
                Password2TextBox.ForeColor = System.Drawing.Color.Black;
                lblResult.Text = "";
            }
            else
            {
                string hashedPsw = CryptoLib.Encryptor.MD5Hash(Password1TextBox.Text);
                User.Instance.users.Add(UserNameTextBox.Text, hashedPsw);
                lblError.Text = "";
                Password1TextBox.ForeColor = System.Drawing.Color.Black;
                Password2TextBox.ForeColor = System.Drawing.Color.Black;
                UserNameTextBox.ForeColor = System.Drawing.Color.Black;
                lblResult.ForeColor = System.Drawing.Color.Green;
                lblResult.Text = "Signed Up Succesfully.";
            }
        }

        private void UserNameTextBox_Enter(object sender, EventArgs e)
        {
            if (UserNameTextBox.Text == "Username")
            {
                UserNameTextBox.Text = "";
                UserNameTextBox.ForeColor = Color.Black;
            }
        }

        private void UserNameTextBox_Leave(object sender, EventArgs e)
        {
            if (UserNameTextBox.Text == "")
            {
                UserNameTextBox.Text = "Username";
                UserNameTextBox.ForeColor = Color.DimGray;
            }
        }

        private void Password1TextBox_Enter(object sender, EventArgs e)
        {
            if (Password1TextBox.Text == "Password")
            {
                Password1TextBox.Text = "";
                Password1TextBox.ForeColor = Color.Black;
            }
        }

        private void Password1TextBox_Leave(object sender, EventArgs e)
        {
            if (Password1TextBox.Text == "")
            {
                Password1TextBox.Text = "Password";
                Password1TextBox.ForeColor = Color.DimGray;
            }
        }

        private void Password2TextBox_Enter(object sender, EventArgs e)
        {
            if (Password2TextBox.Text == "Password")
            {
                Password2TextBox.Text = "";
                Password2TextBox.ForeColor = Color.Black;
            }
        }

        private void Password2TextBox_Leave(object sender, EventArgs e)
        {
            if (Password2TextBox.Text == "")
            {
                Password2TextBox.Text = "Password";
                Password2TextBox.ForeColor = Color.DimGray;
            }
        }

        private void SignUp_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Login login = new Login();
            this.Hide();
            login.ShowDialog();
        }

        private void UserNameTextBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblError_Click(object sender, EventArgs e)
        {

        }
        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
