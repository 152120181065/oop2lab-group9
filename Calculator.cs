﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_lab1
{
    public partial class Calculator : Form
    {
        private static Calculator inst;
        public static Calculator GetForm
        {
            get
            {
                if(inst==null || inst.IsDisposed)
                    inst= new Calculator();
                return inst;

            }
        }
        public Calculator()
        {     
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        { }

        private void btnad_Click(object sender, EventArgs e)
        {
            btnad.BackColor = Color.Honeydew;
            btnsub.BackColor = Color.Empty;
            btnmp.BackColor = Color.Empty;
            btndv.BackColor = Color.Empty;

            float nr1 = 0;
            float nr2 = 0;
            nr1 = float.Parse(txt1.Text);
            nr2 = float.Parse(txt2.Text);
            float result = 0;
            result = nr1 + nr2;
            lbresult.Text = result.ToString();
            if (result > 0)
                lbresult.BackColor = Color.Green;
            else
                lbresult.BackColor = Color.Red;
        }

        private void btnsub_Click(object sender, EventArgs e)
        {
            btnsub.BackColor = Color.Honeydew;
            btnad.BackColor = Color.Empty;
            btnmp.BackColor = Color.Empty;
            btndv.BackColor = Color.Empty;

            float nr1 = 0;
            float nr2 = 0;
            nr1 = float.Parse(txt1.Text);
            nr2 = float.Parse(txt2.Text);
            float result = 0;
            result = nr1 - nr2;
            lbresult.Text = result.ToString();
            if (result > 0)
                lbresult.BackColor = Color.Green;
            else
                lbresult.BackColor = Color.Red;
        }

        private void btnmp_Click(object sender, EventArgs e)
        {
            btnmp.BackColor = Color.Honeydew;
            btnsub.BackColor = Color.Empty;
            btnad.BackColor = Color.Empty;
            btndv.BackColor = Color.Empty;

            float nr1 = 0;
            float nr2 = 0;
            nr1 = float.Parse(txt1.Text);
            nr2 = float.Parse(txt2.Text);
            float result = 0;
            result = nr1 * nr2;
            lbresult.Text = result.ToString();
            if (result > 0)
                lbresult.BackColor = Color.Green;
            else 
                lbresult.BackColor = Color.Red;
        }

        private void btndv_Click(object sender, EventArgs e)
        {
            btndv.BackColor = Color.Honeydew;
            btnsub.BackColor = Color.Empty;
            btnmp.BackColor = Color.Empty;
            btnad.BackColor = Color.Empty;

            float nr1 = 0;
            float nr2 = 0;
            nr1 = float.Parse(txt1.Text);
            nr2 = float.Parse(txt2.Text);
            float result = 0;
            if (nr2 == 0)
            {
                lbresult.Text = "Error! Number can't\nbe  divided by 0.";
                lbresult.BackColor = Color.Red;
            }       
            else
            {
                result = nr1 / nr2; 
                lbresult.Text = result.ToString();
                if (result > 0)
                    lbresult.BackColor = Color.Green;
                else
                    lbresult.BackColor = Color.Red;
            }
            
        }

        private void btnad_MouseHover(object sender, EventArgs e)
        {
            btnad.ForeColor = Color.Red;
        }

        private void btnad_MouseLeave(object sender, EventArgs e)
        {
            btnad.ForeColor = Color.Empty;
        }

        private void btnsub_MouseHover(object sender, EventArgs e)
        {
            btnsub.ForeColor = Color.Red;
        }

        private void btnsub_MouseLeave(object sender, EventArgs e)
        {
            btnsub.ForeColor = Color.Empty;
        }

        private void btnmp_MouseHover(object sender, EventArgs e)
        {
            btnmp.ForeColor = Color.Red;
        }

        private void btnmp_MouseLeave(object sender, EventArgs e)
        {
            btnmp.ForeColor = Color.Empty;
        }

        private void btndv_MouseHover(object sender, EventArgs e)
        {
            btndv.ForeColor = Color.Red;
        }

        private void btndv_MouseLeave(object sender, EventArgs e)
        {
            btndv.ForeColor = Color.Empty;
        }

        private void Calculator_Load(object sender, EventArgs e)
        {

        }

        private void Calculator_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void txt1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
