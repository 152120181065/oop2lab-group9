﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace oop_lab1
{
    public partial class Encryptor : Form
    {
        private static Encryptor inst;
        public static Encryptor GetForm
        {
            get
            {
                if (inst == null || inst.IsDisposed)
                    inst = new Encryptor();
                return inst;

            }
        }

        public Encryptor()
        {
            InitializeComponent();
        }

        //text changelerde ve en/decrpt işlemlerinde kullanılması için local variable tanımladım.
        int algoritmaTuru = -1; // 0 = sezar şifreleme, 1 = vingenere, -1 = hiç biri seçilmedi
        int islemTuru = -1; // 0 = encrypt, 1 = decrypt, -1 = hiç seçilmedi 
        string alphabetArray = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        string inputArray = "";
        string rotOrKeyValue = "";
        bool flag = true; // inputtaki harflerin alfabe dizininde olmasını kontrol eder.
        private string encrypt_caesar(string alphabetArray, string inputArray, int rotValue)
        {
            //if (algoritma_RotKey_UyumKontrol(rotOrKeyValue) == false)
            //{
            //    labelUyari.Text = "rot/key value must be related with the algorithm.\nSaesar - Int, Vingenere - Letter";
            //    return " " ;
            //}
            string output = "";

            //int rotValue = Int32.Parse(rotOrKeyValue);
            for (int j = 0; j < inputArray.Length; j++)
            {
                flag = false;
                for (int i = 0; i < alphabetArray.Length; i++)
                {
                    if (inputArray[j] == alphabetArray[i])
                    {
                        flag = true;
                        if (rotValue >= 0) // rot pozitifse
                        {
                            if (rotValue <= alphabetArray.Length -1- i)
                                output += alphabetArray[i + rotValue];
                            else
                                output += alphabetArray[rotValue - (alphabetArray.Length-i)];
                        }
                        else // rot negatifse
                        {
                            if ((-1)*rotValue <= i) output += alphabetArray[i + rotValue];
                            else
                                output += alphabetArray[(alphabetArray.Length-1)-((-1)*rotValue-(i+1))];

                        }
                        break; // inputtaki harfi alfabede bulup öteledik ve sıradaki input harfine geçicez
                    }

                }
                if (flag == false)  // inputtaki harflerden biri alfabede bulunamadı
                {
                    output = "";
                    break;
                }
            }

            return output;
        }

        private string decrypt_caesar(string alphabetArray, string inputArray, int rotValue)
        {

            string output = encrypt_caesar(alphabetArray, inputArray, (-1) * rotValue);
            return output;
        }

        private string encrypt_vingenere(string alphabetArray, string inputArray)
        {
            string output = "";
            int countRotKeyValasd = 0;
            int rotValue = 0;
            for (int j = 0; j < inputArray.Length; j++)
            {
                flag = false;
                for (int i = 0; i < alphabetArray.Length; i++)
                {

                    if (rotOrKeyValue[countRotKeyValasd % rotOrKeyValue.Length] == alphabetArray[i])
                    {
                        flag = true;
                        rotValue = i + 1;
                        //string inputLetter = new string (inputArray[j]);
                        output += encrypt_caesar(alphabetArray, Char.ToString(inputArray[j]), rotValue);
                    }
                }
                if (flag == false)  // inputtaki harflerden biri alfabede bulunamadı
                {
                    output = "";
                    break;
                }
                countRotKeyValasd++;
            }
                return output;
        }

        private string decrypt_vingenere(string alphabetArray, string inputArray)
        {

            string output = "";
            int countRotKeyValasd = 0;
            int rotValue = 0;
            for (int j = 0; j < inputArray.Length; j++)
            {
                flag = false;
                for (int i = 0; i < alphabetArray.Length; i++)
                {

                    if (rotOrKeyValue[countRotKeyValasd % rotOrKeyValue.Length] == alphabetArray[i])
                    {
                        flag = true;
                        rotValue = i + 1;
                        //string inputLetter = new string (inputArray[j]);
                        output += encrypt_caesar(alphabetArray, Char.ToString(inputArray[j]), (-1) * rotValue);
                    }
                }
                if (flag == false)  // inputtaki harflerden biri alfabede bulunamadı
                {
                    output = "";
                    break;
                }
                countRotKeyValasd++;
            }
            return output;
        }

        private void Encryptor_Load(object sender, EventArgs e)
        {
           
             
        }
        private bool algoritma_RotKey_UyumKontrol (string x)
        {
            for (int i=0; i<x.Length; i++)
            {
                if(algoritmaTuru == 0) // sezarda int olmalı
                {
                    int numericValue;
                    if ( int.TryParse(x, out numericValue) == false) return false;
                }
                else if(algoritmaTuru==1) // vingenere letter olmalı 
                {
                    if (Char.IsLetter(x[i]) == false) return false;
                }
            }
            return true;
        }
       
        private void txtAlphabet_TextChanged(object sender, EventArgs e)
        {
           alphabetArray = txtAlphabet.Text;

        }

        private void btnEncryption_CheckedChanged(object sender, EventArgs e)
        {
            islemTuru = 0;
        }

        private void btnDecryption_CheckedChanged(object sender, EventArgs e)
        {
            islemTuru = 1;
        }

        private void btnCeaser_CheckedChanged(object sender, EventArgs e)
        {
            algoritmaTuru = 0;
        }

        private void btnVigenere_CheckedChanged(object sender, EventArgs e)
        {
            algoritmaTuru = 1;
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            inputArray = txtInput.Text;
        }

        private void lblAlphabet_Click(object sender, EventArgs e)
        {

        }

        private void lblInput_Click(object sender, EventArgs e)
        {

        }

        private void btnRun_Click(object sender, EventArgs e)
        {
            //lblResulttxt.Text = alphabetArray + "  Atur:" + algoritmaTuru + "  Itur" + islemTuru + "  " + inputArray + "  rotKey:" + rotOrKeyValue + " " + algoritma_RotKey_UyumKontrol(rotOrKeyValue);
            //  if (algoritma_RotKey_UyumKontrol(rotOrKeyValue) == false) labelUyari.Text = "rot/key value must be related with the algorithm.\nSaesar - Int, Vingenere - Letter";
            labelResult.Text = String.Empty;
            labelUyari.Text = String.Empty;

            if (algoritma_RotKey_UyumKontrol(rotOrKeyValue) == false)
            {
                labelUyari.Text = "rot/key value must be related with the algorithm.\nSaesar - Int, Vingenere - Letter";
               // return " ";
            }
            else
            {
                if (algoritmaTuru == 0 && islemTuru == 0)
                {
                    int rotValue = Int32.Parse(rotOrKeyValue);
                    labelResult.Text = encrypt_caesar(alphabetArray, inputArray, rotValue);
                }
                else if (algoritmaTuru == 0 && islemTuru == 1)
                {
                    int rotValue = Int32.Parse(rotOrKeyValue);
                    labelResult.Text = decrypt_caesar(alphabetArray, inputArray, rotValue);
                }
                else if(algoritmaTuru == 1 && islemTuru == 0)
                {
                    labelResult.Text = encrypt_vingenere(alphabetArray, inputArray);
                }
                else if (algoritmaTuru == 1 && islemTuru == 1)
                {
                    labelResult.Text = decrypt_vingenere(alphabetArray, inputArray);
                }
                if (flag == false) // input harflerinden biri alfabede yer almıyor.
                {
                    labelUyari.Text = "Input has character(s) which the alphabet does not include.";
                }
            }
            
        }

        private void lblResulttxt_Click(object sender, EventArgs e)
        {
            
        }

        private void grpAlgorithm_Enter(object sender, EventArgs e)
        {

        }

        private void grpEncryption_Enter(object sender, EventArgs e)
        {

        }
        private void Encryptor_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void Encryptor_Load_1(object sender, EventArgs e)
        {

        }



        private void txtAlphabet_Leave(object sender, EventArgs e)
        {

            if (txtAlphabet.Text == "")
            {
                txtAlphabet.Text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

                txtAlphabet.ForeColor = Color.Gray;
            }
        }

        private void txtAlphabet_Enter(object sender, EventArgs e)
        {
            if (txtAlphabet.Text == "ABCDEFGHIJKLMNOPQRSTUVWXYZ")
            {
                txtAlphabet.Text = "";

                txtAlphabet.ForeColor = Color.Black;
            }
               
        }

        private void textRotOrKey_TextChanged(object sender, EventArgs e)
        {
            rotOrKeyValue = textRotOrKey.Text;
        }

        private void label1_Click (object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void txtAlphabet_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsSeparator(e.KeyChar);
        }

        private void txtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsLetter(e.KeyChar) && !char.IsControl(e.KeyChar) && !char.IsSeparator(e.KeyChar);
        }

        private void labelResult_Click(object sender, EventArgs e)
        {

        }

        //private void label1_Click_2(object sender, EventArgs e)
        //{

        //}
    }
}
