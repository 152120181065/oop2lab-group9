﻿
namespace oop_lab1
{
    partial class Calculator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt1 = new System.Windows.Forms.TextBox();
            this.txt2 = new System.Windows.Forms.TextBox();
            this.btnad = new System.Windows.Forms.Button();
            this.btnsub = new System.Windows.Forms.Button();
            this.btnmp = new System.Windows.Forms.Button();
            this.btndv = new System.Windows.Forms.Button();
            this.lbresult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt1
            // 
            this.txt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt1.Location = new System.Drawing.Point(37, 34);
            this.txt1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(152, 30);
            this.txt1.TabIndex = 0;
            this.txt1.TextChanged += new System.EventHandler(this.txt1_TextChanged);
            // 
            // txt2
            // 
            this.txt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.txt2.Location = new System.Drawing.Point(37, 86);
            this.txt2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(152, 30);
            this.txt2.TabIndex = 1;
            this.txt2.TextChanged += new System.EventHandler(this.txt2_TextChanged);
            // 
            // btnad
            // 
            this.btnad.Cursor = System.Windows.Forms.Cursors.Default;
            this.btnad.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnad.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnad.Location = new System.Drawing.Point(253, 231);
            this.btnad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnad.Name = "btnad";
            this.btnad.Size = new System.Drawing.Size(132, 38);
            this.btnad.TabIndex = 2;
            this.btnad.Text = "Add (+)";
            this.btnad.UseVisualStyleBackColor = true;
            this.btnad.Click += new System.EventHandler(this.btnad_Click);
            this.btnad.MouseLeave += new System.EventHandler(this.btnad_MouseLeave);
            this.btnad.MouseHover += new System.EventHandler(this.btnad_MouseHover);
            // 
            // btnsub
            // 
            this.btnsub.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnsub.Location = new System.Drawing.Point(59, 231);
            this.btnsub.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnsub.Name = "btnsub";
            this.btnsub.Size = new System.Drawing.Size(132, 38);
            this.btnsub.TabIndex = 3;
            this.btnsub.Text = "Subtract (-)";
            this.btnsub.UseVisualStyleBackColor = true;
            this.btnsub.Click += new System.EventHandler(this.btnsub_Click);
            this.btnsub.MouseLeave += new System.EventHandler(this.btnsub_MouseLeave);
            this.btnsub.MouseHover += new System.EventHandler(this.btnsub_MouseHover);
            // 
            // btnmp
            // 
            this.btnmp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btnmp.Location = new System.Drawing.Point(59, 156);
            this.btnmp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnmp.Name = "btnmp";
            this.btnmp.Size = new System.Drawing.Size(132, 38);
            this.btnmp.TabIndex = 4;
            this.btnmp.Text = "Multiply (*)";
            this.btnmp.UseVisualStyleBackColor = true;
            this.btnmp.Click += new System.EventHandler(this.btnmp_Click);
            this.btnmp.MouseLeave += new System.EventHandler(this.btnmp_MouseLeave);
            this.btnmp.MouseHover += new System.EventHandler(this.btnmp_MouseHover);
            // 
            // btndv
            // 
            this.btndv.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.btndv.Location = new System.Drawing.Point(253, 156);
            this.btndv.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btndv.Name = "btndv";
            this.btndv.Size = new System.Drawing.Size(132, 38);
            this.btndv.TabIndex = 5;
            this.btndv.Text = "Divide (/)";
            this.btndv.UseVisualStyleBackColor = true;
            this.btndv.Click += new System.EventHandler(this.btndv_Click);
            this.btndv.MouseLeave += new System.EventHandler(this.btndv_MouseLeave);
            this.btndv.MouseHover += new System.EventHandler(this.btndv_MouseHover);
            // 
            // lbresult
            // 
            this.lbresult.AutoSize = true;
            this.lbresult.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.lbresult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbresult.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lbresult.ForeColor = System.Drawing.Color.Yellow;
            this.lbresult.Location = new System.Drawing.Point(237, 60);
            this.lbresult.Name = "lbresult";
            this.lbresult.Size = new System.Drawing.Size(87, 31);
            this.lbresult.TabIndex = 6;
            this.lbresult.Text = "Result";
            this.lbresult.Click += new System.EventHandler(this.label1_Click);
            // 
            // Calculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 309);
            this.Controls.Add(this.lbresult);
            this.Controls.Add(this.btndv);
            this.Controls.Add(this.btnmp);
            this.Controls.Add(this.btnsub);
            this.Controls.Add(this.btnad);
            this.Controls.Add(this.txt2);
            this.Controls.Add(this.txt1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Calculator";
            this.Text = "Windows Calculator";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Calculator_FormClosed);
            this.Load += new System.EventHandler(this.Calculator_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt1;
        private System.Windows.Forms.TextBox txt2;
        private System.Windows.Forms.Button btnad;
        private System.Windows.Forms.Button btnsub;
        private System.Windows.Forms.Button btnmp;
        private System.Windows.Forms.Button btndv;
        private System.Windows.Forms.Label lbresult;
    }
}

