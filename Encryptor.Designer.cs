﻿namespace oop_lab1
{
    partial class Encryptor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpAlgorithm = new System.Windows.Forms.GroupBox();
            this.btnVigenere = new System.Windows.Forms.RadioButton();
            this.btnCeaser = new System.Windows.Forms.RadioButton();
            this.btnEncryption = new System.Windows.Forms.RadioButton();
            this.btnDecryption = new System.Windows.Forms.RadioButton();
            this.grpEncryption = new System.Windows.Forms.GroupBox();
            this.txtAlphabet = new System.Windows.Forms.TextBox();
            this.lblAlphabet = new System.Windows.Forms.Label();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.lblInput = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.lblResulttxt = new System.Windows.Forms.Label();
            this.btnRun = new System.Windows.Forms.Button();
            this.rotOrKey = new System.Windows.Forms.Label();
            this.textRotOrKey = new System.Windows.Forms.TextBox();
            this.labelUyari = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.grpAlgorithm.SuspendLayout();
            this.grpEncryption.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpAlgorithm
            // 
            this.grpAlgorithm.Controls.Add(this.btnVigenere);
            this.grpAlgorithm.Controls.Add(this.btnCeaser);
            this.grpAlgorithm.Location = new System.Drawing.Point(319, 15);
            this.grpAlgorithm.Margin = new System.Windows.Forms.Padding(4);
            this.grpAlgorithm.Name = "grpAlgorithm";
            this.grpAlgorithm.Padding = new System.Windows.Forms.Padding(4);
            this.grpAlgorithm.Size = new System.Drawing.Size(177, 123);
            this.grpAlgorithm.TabIndex = 1;
            this.grpAlgorithm.TabStop = false;
            this.grpAlgorithm.Text = "Algorithm";
            this.grpAlgorithm.Enter += new System.EventHandler(this.grpAlgorithm_Enter);
            // 
            // btnVigenere
            // 
            this.btnVigenere.AutoSize = true;
            this.btnVigenere.Location = new System.Drawing.Point(8, 74);
            this.btnVigenere.Margin = new System.Windows.Forms.Padding(4);
            this.btnVigenere.Name = "btnVigenere";
            this.btnVigenere.Size = new System.Drawing.Size(86, 21);
            this.btnVigenere.TabIndex = 1;
            this.btnVigenere.TabStop = true;
            this.btnVigenere.Text = "Vigenère";
            this.btnVigenere.UseVisualStyleBackColor = true;
            this.btnVigenere.CheckedChanged += new System.EventHandler(this.btnVigenere_CheckedChanged);
            // 
            // btnCeaser
            // 
            this.btnCeaser.AutoSize = true;
            this.btnCeaser.Location = new System.Drawing.Point(8, 34);
            this.btnCeaser.Margin = new System.Windows.Forms.Padding(4);
            this.btnCeaser.Name = "btnCeaser";
            this.btnCeaser.Size = new System.Drawing.Size(74, 21);
            this.btnCeaser.TabIndex = 0;
            this.btnCeaser.TabStop = true;
            this.btnCeaser.Text = "Ceaser";
            this.btnCeaser.UseVisualStyleBackColor = true;
            this.btnCeaser.CheckedChanged += new System.EventHandler(this.btnCeaser_CheckedChanged);
            // 
            // btnEncryption
            // 
            this.btnEncryption.AutoSize = true;
            this.btnEncryption.Location = new System.Drawing.Point(8, 34);
            this.btnEncryption.Margin = new System.Windows.Forms.Padding(4);
            this.btnEncryption.Name = "btnEncryption";
            this.btnEncryption.Size = new System.Drawing.Size(96, 21);
            this.btnEncryption.TabIndex = 0;
            this.btnEncryption.TabStop = true;
            this.btnEncryption.Text = "Encryption";
            this.btnEncryption.UseVisualStyleBackColor = true;
            this.btnEncryption.CheckedChanged += new System.EventHandler(this.btnEncryption_CheckedChanged);
            // 
            // btnDecryption
            // 
            this.btnDecryption.AutoSize = true;
            this.btnDecryption.Location = new System.Drawing.Point(8, 74);
            this.btnDecryption.Margin = new System.Windows.Forms.Padding(4);
            this.btnDecryption.Name = "btnDecryption";
            this.btnDecryption.Size = new System.Drawing.Size(97, 21);
            this.btnDecryption.TabIndex = 1;
            this.btnDecryption.TabStop = true;
            this.btnDecryption.Text = "Decryption";
            this.btnDecryption.UseVisualStyleBackColor = true;
            this.btnDecryption.CheckedChanged += new System.EventHandler(this.btnDecryption_CheckedChanged);
            // 
            // grpEncryption
            // 
            this.grpEncryption.Controls.Add(this.btnDecryption);
            this.grpEncryption.Controls.Add(this.btnEncryption);
            this.grpEncryption.Location = new System.Drawing.Point(41, 15);
            this.grpEncryption.Margin = new System.Windows.Forms.Padding(4);
            this.grpEncryption.Name = "grpEncryption";
            this.grpEncryption.Padding = new System.Windows.Forms.Padding(4);
            this.grpEncryption.Size = new System.Drawing.Size(161, 123);
            this.grpEncryption.TabIndex = 0;
            this.grpEncryption.TabStop = false;
            this.grpEncryption.Text = "De/En-cryption";
            this.grpEncryption.Enter += new System.EventHandler(this.grpEncryption_Enter);
            // 
            // txtAlphabet
            // 
            this.txtAlphabet.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtAlphabet.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtAlphabet.Location = new System.Drawing.Point(136, 182);
            this.txtAlphabet.Margin = new System.Windows.Forms.Padding(4);
            this.txtAlphabet.MaxLength = 50;
            this.txtAlphabet.Name = "txtAlphabet";
            this.txtAlphabet.Size = new System.Drawing.Size(241, 22);
            this.txtAlphabet.TabIndex = 3;
            this.txtAlphabet.Text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            this.txtAlphabet.TextChanged += new System.EventHandler(this.txtAlphabet_TextChanged);
            this.txtAlphabet.Enter += new System.EventHandler(this.txtAlphabet_Enter);
            this.txtAlphabet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAlphabet_KeyPress);
            this.txtAlphabet.Leave += new System.EventHandler(this.txtAlphabet_Leave);
            // 
            // lblAlphabet
            // 
            this.lblAlphabet.AutoSize = true;
            this.lblAlphabet.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlphabet.Location = new System.Drawing.Point(19, 177);
            this.lblAlphabet.Margin = new System.Windows.Forms.Padding(0);
            this.lblAlphabet.Name = "lblAlphabet";
            this.lblAlphabet.Size = new System.Drawing.Size(106, 36);
            this.lblAlphabet.TabIndex = 2;
            this.lblAlphabet.Text = "Alphabet:";
            this.lblAlphabet.Click += new System.EventHandler(this.lblAlphabet_Click);
            // 
            // txtInput
            // 
            this.txtInput.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtInput.Location = new System.Drawing.Point(139, 284);
            this.txtInput.Margin = new System.Windows.Forms.Padding(4);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(241, 22);
            this.txtInput.TabIndex = 5;
            this.txtInput.TextChanged += new System.EventHandler(this.txtInput_TextChanged);
            this.txtInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtInput_KeyPress);
            // 
            // lblInput
            // 
            this.lblInput.AutoSize = true;
            this.lblInput.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInput.Location = new System.Drawing.Point(55, 270);
            this.lblInput.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblInput.Name = "lblInput";
            this.lblInput.Size = new System.Drawing.Size(70, 36);
            this.lblInput.TabIndex = 4;
            this.lblInput.Text = "Input:";
            this.lblInput.Click += new System.EventHandler(this.lblInput_Click);
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.CausesValidation = false;
            this.lblResult.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.lblResult.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblResult.ForeColor = System.Drawing.Color.DarkRed;
            this.lblResult.Location = new System.Drawing.Point(131, 304);
            this.lblResult.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 29);
            this.lblResult.TabIndex = 8;
            // 
            // lblResulttxt
            // 
            this.lblResulttxt.AutoSize = true;
            this.lblResulttxt.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResulttxt.Location = new System.Drawing.Point(46, 320);
            this.lblResulttxt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblResulttxt.Name = "lblResulttxt";
            this.lblResulttxt.Size = new System.Drawing.Size(79, 36);
            this.lblResulttxt.TabIndex = 7;
            this.lblResulttxt.Text = "Result:";
            this.lblResulttxt.Click += new System.EventHandler(this.lblResulttxt_Click);
            // 
            // btnRun
            // 
            this.btnRun.BackColor = System.Drawing.Color.OrangeRed;
            this.btnRun.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRun.Font = new System.Drawing.Font("Myanmar Text", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRun.ForeColor = System.Drawing.Color.Snow;
            this.btnRun.Location = new System.Drawing.Point(423, 210);
            this.btnRun.Margin = new System.Windows.Forms.Padding(4);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(135, 60);
            this.btnRun.TabIndex = 6;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // rotOrKey
            // 
            this.rotOrKey.AutoSize = true;
            this.rotOrKey.Font = new System.Drawing.Font("Myanmar Text", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rotOrKey.Location = new System.Drawing.Point(28, 223);
            this.rotOrKey.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.rotOrKey.Name = "rotOrKey";
            this.rotOrKey.Size = new System.Drawing.Size(97, 36);
            this.rotOrKey.TabIndex = 9;
            this.rotOrKey.Text = "Rot/Key:";
            this.rotOrKey.Click += new System.EventHandler(this.label1_Click);
            // 
            // textRotOrKey
            // 
            this.textRotOrKey.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.textRotOrKey.Location = new System.Drawing.Point(136, 228);
            this.textRotOrKey.Name = "textRotOrKey";
            this.textRotOrKey.Size = new System.Drawing.Size(241, 22);
            this.textRotOrKey.TabIndex = 10;
            this.textRotOrKey.TextChanged += new System.EventHandler(this.textRotOrKey_TextChanged);
            // 
            // labelUyari
            // 
            this.labelUyari.AutoSize = true;
            this.labelUyari.ForeColor = System.Drawing.Color.Red;
            this.labelUyari.Location = new System.Drawing.Point(133, 362);
            this.labelUyari.Name = "labelUyari";
            this.labelUyari.Size = new System.Drawing.Size(0, 17);
            this.labelUyari.TabIndex = 11;
            this.labelUyari.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(139, 328);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(0, 17);
            this.labelResult.TabIndex = 12;
            this.labelResult.Click += new System.EventHandler(this.labelResult_Click);
            // 
            // Encryptor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(599, 388);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelUyari);
            this.Controls.Add(this.textRotOrKey);
            this.Controls.Add(this.rotOrKey);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.lblResulttxt);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblInput);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.lblAlphabet);
            this.Controls.Add(this.txtAlphabet);
            this.Controls.Add(this.grpEncryption);
            this.Controls.Add(this.grpAlgorithm);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Encryptor";
            this.Text = "D/Encryptor";
            this.Load += new System.EventHandler(this.Encryptor_Load_1);
            this.grpAlgorithm.ResumeLayout(false);
            this.grpAlgorithm.PerformLayout();
            this.grpEncryption.ResumeLayout(false);
            this.grpEncryption.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox grpAlgorithm;
        private System.Windows.Forms.RadioButton btnEncryption;
        private System.Windows.Forms.RadioButton btnDecryption;
        private System.Windows.Forms.GroupBox grpEncryption;
        private System.Windows.Forms.RadioButton btnVigenere;
        private System.Windows.Forms.RadioButton btnCeaser;
        private System.Windows.Forms.TextBox txtAlphabet;
        private System.Windows.Forms.Label lblAlphabet;
        private System.Windows.Forms.TextBox txtInput;
        private System.Windows.Forms.Label lblInput;
        public System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label lblResulttxt;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.Label rotOrKey;
        private System.Windows.Forms.TextBox textRotOrKey;
        private System.Windows.Forms.Label labelUyari;
        private System.Windows.Forms.Label labelResult;
    }
}